BAC
=======
BAC is a scalable Bagged Associative Classifier built on Apache Spark.

The algorithm builds multiple associative classifiers in parallel, finally combining them in a single model by exploiting [Bagging](https://en.wikipedia.org/wiki/Bootstrap_aggregating).

There are three main parameters to set:
* the number of models to train in parallel, `numModels`
* the fraction of dataset to sample for each model `sampleSize` and 
* the minimum support threshold `minSupport`.


## Usage in a Spark Application

This section assumes you are already confident with writing a Spark application.
If not, go to Standalone usage.

The example below shows how to load a comma-separated file and train a BAC model with the given parameters.
The test error is calculated to measure the algorithm accuracy.
Note that each record is an array of `Long`, and the least integer in the row is used as class label, while all the other items are used as features.

```scala
import it.polito.dbdmg.ml.L3Ensemble

// Load and parse the data file.
val inf = sc.textFile(inputFile)
val data = inf.map(_.split(",").map(_.toLong))
// Split the data into training and test sets (30% held out for testing)
val splits = data.randomSplit(Array(0.7, 0.3))
val (trainingData, testData) = (splits(0), splits(1))

// Train a BAC model.
val numModels = 10
val numClasses = 3
val minSupport = 0.1
val sampleSize = 0.1

val model = new L3Ensemble(numModels = numModels, numClasses = numClasses, minSupport = minSupport, sampleSize = sampleSize)

// Evaluate model on test instances and compute test error
val labelAndPreds = testData.map { point =>
  label = point.min //predict uses the least integer in the row as class label,
  val prediction = model.predict(point) //so be sure your record is structured accordingly
  (point.label, prediction)
}
val testErr = labelAndPreds.filter(r => r._1 != r._2).count.toDouble / testData.count()
println("Test Error = " + testErr)
println("Learned BAC model:\n" + model.toDebugString)
```

## Standalone usage
### Compile

The project uses sbt to compile & build the BAC tool. The following command downloads the dependencies, compiles the code, and creates the required jar file in the `target` directory.

	sbt package

The generated jar file, containing BAC, is in `target/scala-2.10/bac_2.10-0.2.jar`

The jar file bac_2.10-0.2.jar includes also all the needed dependencies. Hence, no libraries need to be uploaded on the Hadoop cluster in order to run BAC.

Please note that the implementation is developed and tested on 2.5.0-cdh5.3.1 Cloudera configuration.

### Input dataset format

The input dataset for the class BacUciCv is a list of records with n+1 columns separated by comma, where n is the number of features and the last column is the class label of the record.
The first line can be optionally an header, marked by an initial `|`.
The following is an example extracted from the IRIS dataset converted to this format and discretized, which is available online at https://www.sgi.com/tech/mlc/db/ together with other UCI datasets:

    |Discretization into bins of size 0
    5\.85-Inf, 3\.05-Inf, 2\.6-4\.85, 0\.75-1\.65, Iris-versicolor.
    5\.45-5\.85, 3\.05-Inf, -Inf-2\.6, -Inf-0\.75, Iris-setosa.
    5\.85-Inf, 3\.05-Inf, 4\.85-Inf, 1\.65-Inf, Iris-virginica.
    5\.85-Inf, -Inf-3\.05, 2\.6-4\.85, 0\.75-1\.65, Iris-versicolor.


### Run

The class BacUciCv executes a 10-fold cross-validation of the BAC algorithm on a given input dataset, and outputs the results of the cross-validation on stdout.

To run it, be sure of having a discretized dataset in the above-mentioned format, let's say `iris.data`, and then run:

    spark-submit --master local[*] --class BacUciCv bac_2.10-0.2.jar irisd.data 10 0.1 0.1

where the last three parameters are, in order, the number of models to generate, the fraction of dataset to sample for each model and the minimum support threshold.

Of course the parameters of `spark-submit` change depending on your cluster configuration. Here we have set a local run with all the cores available (`--master local[*]`).

	
## References

The work was first presented here:

> Venturini, Garza, Apiletti (2016)
**BAC: A bagged associative classifier for big data frameworks**. In: 3rd International Workshop on Big Data Applications and Principles, BigDap 2016, co-located with the 20th East-European Conference on Advances in Databases and Information Systems, ADBIS 2016, Prague, Czech Republic,
August 28, 2016. pp. 137-146.
Available at: http://porto.polito.it/id/eprint/2651083

## Credits

The code uses part of [MLlib](http://spark.apache.org/mllib/) code, provided under Apache License, Version 2.0.